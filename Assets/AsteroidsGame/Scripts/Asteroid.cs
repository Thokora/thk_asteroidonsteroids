using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : ExplotionBehaviour
{
    [Space, Header("Asteroids Settings")]
    [SerializeField] private AsteroidType asteroidType;
    [SerializeField] private int fragmenstForBig, fragmenstForMedium, fragmenstForLittle;
    [Space, SerializeField] private List<GameObject> fragments;
    [SerializeField] private float tumble = 0.25f;
    [SerializeField] private float xMinRandomValue, xMaxRandomValue;
    [SerializeField] private float yMinRandomValue, yMaxRandomValue;

    private Transform asteroidParent;

    protected override void OnEnable()
    {
        base.OnEnable();
        DeactiveFragments();
        rb.angularVelocity = Random.insideUnitSphere* tumble;
        direction = new Vector3(Random.Range(xMinRandomValue, xMaxRandomValue), Random.Range(yMinRandomValue, yMaxRandomValue), direction.z);
    }

    public void Initialize (Transform asteroidsConteiner)
    {
        asteroidParent = asteroidsConteiner;
    }

    protected override void InteractWithTag(string tagImpacted)
    {
        base.InteractWithTag(tagImpacted);

        if (tagImpacted == "Player")
        {

        }
    }

    protected override void OnObjectImpacted(bool showExplosionEffect)
    {
        base.OnObjectImpacted(showExplosionEffect);

        if (currentImpacts == impactToExploit)
            ScoreManager.AddScore();

        switch (asteroidType)
        {
            case AsteroidType.Big:
                ActiveFragments(fragmenstForBig);
                break;
            case AsteroidType.Medium:
                ActiveFragments(fragmenstForMedium);
                break;
            case AsteroidType.Little:
                ActiveFragments(fragmenstForLittle);
                break;
        }
    }

    private void ActiveFragments(int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            if (i < fragments.Count)
            {
                fragments[i].SetActive(true);

                if (asteroidParent != null)
                    fragments[i].transform.parent = asteroidParent;
                else
                    fragments[i].transform.parent = null;
            }
        }
    }

    private void DeactiveFragments()
    {
        for (int i = 0; i < fragments.Count; i++)
        {
            fragments[i].SetActive(false);
        }
    }
}
