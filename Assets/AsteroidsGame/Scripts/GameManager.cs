﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private GameState currenGameState;

    [SerializeField] private PlayerShipController _playerShipController;
    [SerializeField] private WaveController _waveController;
    [SerializeField] private ScoreUIBehaviour _scoreUIBehaviour;
    [SerializeField] private InstructionBehaviour _instructionBehaviour;

    private void Awake()
    {
        ChangeGameState(GameState.Initialization);
    }

    /// <summary>
    /// Use this method to change between diferences game stated
    /// </summary>
    private void ChangeGameState(GameState gameState)
    {
        currenGameState = gameState;
        switch (currenGameState)
        {
            case GameState.Initialization:
                Initialize();
                break;
            case GameState.Instructions:
                Instructions();
                break;
            case GameState.StartGameplay:
                GamePlay();
                break;
            case GameState.EndGameplay:
                EndGamePlay();
                break;
            case GameState.Reset:
                ResetGame();
                break;
            default:
                Debug.Log("Choose a game state first");
                break;
        }
    }

    /// <summary>
    /// Use this method to initialize all controllers, that means to pass references and subscribe to events
    /// </summary>
    private void Initialize()
    {
        Globals.GameStarted = false;

        _playerShipController.Initialize();
        _waveController.Initialize();

        _playerShipController.PlayerDead += _waveController.OnPlayerDead;
        _playerShipController.PlayerDead += _scoreUIBehaviour.OnPlayerDead;
        _waveController.NewWaveStarted += _playerShipController.OnNewWave;

        _scoreUIBehaviour.ResetUIValue();

        _instructionBehaviour.StartGame += GamePlay;

        Instructions();
    }

    /// <summary>
    /// Use this methos to show tutorial or to teach to users how to play the game
    /// </summary>
    private void Instructions()
    {
        _instructionBehaviour.Show(true);
    }

    /// <summary>
    /// Use this methos to start the main gameplay
    /// </summary>
    private void GamePlay()
    {
        _waveController.NewWave();

        Globals.GameStarted = true;
    }

    /// <summary>
    /// Use this method to end the main gameplay, to save data, to show score and everething you want to hapen when the game is finished
    /// </summary>
    private void EndGamePlay()
    {

    }

    /// <summary>
    /// Use this method to unsubscribe all events, clear lists, clear score, and restart gameplay or scene
    /// </summary>
    private void ResetGame()
    {

    }
}
