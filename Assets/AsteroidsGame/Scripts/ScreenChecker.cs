using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenChecker
{
    public bool IsObjectVisible(Camera cameraToCheck, GameObject ObjectToCheck, float screenLimit = 0.1f)
    {
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(cameraToCheck);
        Vector3 point = ObjectToCheck.transform.position;
        foreach (var plane in planes)
        {
            if (plane.GetDistanceToPoint(point) < screenLimit)
                return false;
        }
        return true;
    }
}
