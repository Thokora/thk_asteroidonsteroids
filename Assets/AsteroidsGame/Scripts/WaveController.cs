using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

public class WaveController : MonoBehaviour
{
    public Action NewWaveStarted;

    [SerializeField] PoolCreator _poolCreator;
    [SerializeField] private Asteroid prefabAsteroid;

    [Space, Header("WaveController")]
    [SerializeField, Tooltip("Position from the waves will start")]
    private Transform spawnPos;
    [SerializeField, Tooltip("Time to star wave 0")]
    private float maxWaveTime = 60;
    [SerializeField, Tooltip("Time will never be less than this value")] 
    private float minWaveTime = 30;
    [SerializeField, Tooltip("Time to substract each wave")] 
    private float difficulty = 5f;
    [SerializeField, Tooltip("Wave 0 will start with this objects amount")]
    private int initialAsteroidAmount = 5;
    [SerializeField, Tooltip("Ecah wave this value will be add to initialAsteroidAmount")]
    private int amountToAddPerWave = 5;

    [Space, Header("Canvas Waves")]
    [SerializeField] private GameObject newWavePanel;
    [SerializeField] private Button newWaveButton;
    [SerializeField] private TMP_Text waveTimeText;

    private PlayerShipController _playerShipController;
    private List<Asteroid> asteroids = new List<Asteroid>();
    private int nextWave;
    private float waveTimer;
    private bool waveStarted;

    private void Awake()
    {
        newWavePanel.SetActive(false);
        newWaveButton.onClick.AddListener(NewWave);
    }

    public void Initialize()
    {
        NewWaveStarted += OnNewWaveStarted;

        _poolCreator.SetManually(0, null, prefabAsteroid.gameObject);
        _poolCreator.Initialize();

        for (int i = 0; i < _poolCreator.GetAllObjects().Count; i++)
        {
            asteroids.Add(_poolCreator.GetAllObjects()[i].GetComponent<Asteroid>());
        }
    }

    private void Update()
    {
        if (waveStarted)
        {
            waveTimer += Time.deltaTime;
            waveTimeText.text = "Wave " + nextWave + " ends in" +"\n" + "00:" + (maxWaveTime - waveTimer).ToString("00");
            if (waveTimer >= maxWaveTime)
            {
                waveTimer = 0;
                EndWave();
            }
        }
    }

    public void NewWave()
    {
        newWavePanel.SetActive(false);
        Globals.UI_IsOpen = false;

        NewWaveStarted?.Invoke();

        waveStarted = true;
        StartCoroutine(WaitFoNewWave());

        nextWave++;
    }

    IEnumerator WaitFoNewWave()
    {
        for (int i = 0; i < initialAsteroidAmount; i++)
        {
            GameObject asteroid = _poolCreator.GetObjectFromPool();

            if (asteroid != null)
                asteroid.transform.position = spawnPos.position;
            //ubicarlo en posicion random
            float timeToWait = (maxWaveTime - 10) / initialAsteroidAmount;
            yield return new WaitForSeconds(timeToWait);
        }
    }

    private void EndWave()
    {
        newWavePanel.SetActive(true);
        Globals.UI_IsOpen = true;

        waveStarted = false;
        
        maxWaveTime -= difficulty;
        if (maxWaveTime < minWaveTime)
            maxWaveTime = minWaveTime;

        _poolCreator.DeactiveAllObjects();
        StopAllCoroutines();
        initialAsteroidAmount += amountToAddPerWave;
    }

    public void OnPlayerDead()
    {
        waveStarted = false;
        _poolCreator.DeactiveAllObjects();
        StopAllCoroutines();
    }

    private void OnNewWaveStarted()
    {
        //field to do something internal
    }
}
