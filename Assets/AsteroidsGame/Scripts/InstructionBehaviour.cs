using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class InstructionBehaviour : MonoBehaviour
{
    public Action StartGame;

    [SerializeField] private GameObject instructionsPanel;
    [SerializeField] private Button startButton;

    private void Awake()
    {
        StartGame += OnStartGame;
        startButton.onClick.AddListener(OnStartButtonPressed);
    }

    public void Show(bool show)
    {
        Globals.UI_IsOpen = show;
        instructionsPanel.SetActive(show);
    }

    private void OnStartButtonPressed()
    {
        StartGame?.Invoke();
    }

    private void OnStartGame()
    {
        Show(false);
    }
}
