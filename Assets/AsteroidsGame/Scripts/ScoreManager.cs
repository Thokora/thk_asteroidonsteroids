using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    private ScoreManager instance;

    private static int score = 0;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (this != instance)
            Destroy(this.gameObject);
    }

    public static void AddScore()
    {
        score++;
    }


    public static int GetScore()
    {
        return score;
    }


    public static void ResetScore()
    {
        score = 0;
    }   
}
