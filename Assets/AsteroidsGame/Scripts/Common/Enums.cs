﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    Initialization, 
    Instructions, 
    StartGameplay,
    EndGameplay,
    Reset
}

public enum AsteroidType
{
    Big = 0,
    Medium = 1,
    Little = 2
}

