﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;
using UnityEngine.UI;

[RequireComponent(typeof(PoolCreator))]
public class PlayerShipController : MonoBehaviour
{
    public Action PlayerDead;

    private ScreenChecker _screenChecker;
    private PoolCreator _poolCreator;
    private Animator _animator;
    private int currenPositionIndex = 0;
    private int currentBulletsShooted = 0;
    private float shootingTimer;
    private float reloadingTimer;
    private float shieldTimer;
    private bool shieldActived = false;
    private bool reloadingShield = false;
    private int currentWave = -1;

    [Header("Movement limit setting")]
    [SerializeField] private float screenLimit = 2;
    [SerializeField] private float backSmooth = 0.25f;
    [SerializeField] private GameObject objectToCheckLimit;

    [Space, Header("Fire setting")]
    [SerializeField] private List<string> tagsToCollision = new List<string>();
    [Space]
    [SerializeField] private float fireRate = 0.5f;
    [SerializeField] private float timeToReload = 0.5f;
    [SerializeField] private int maxBullets = 60;

    [Space, Header("Shield setting")]
    [SerializeField] private GameObject shieldObject;
    [SerializeField] private float timeToUseShield = 2f;
    [SerializeField] private float timeToReloadShield = 2f;

    [Space]
    [SerializeField] private float shipSpeed = 2;
    [SerializeField] private List<Transform> shootPositions = new List<Transform>();
    [SerializeField] private List<GameObject> removablesShipParts = new List<GameObject>();
    [SerializeField] private int lives;

    [Space, Header("UI Info")]
    [SerializeField] private List<GameObject> livesImages = new List<GameObject>();
    [SerializeField] private Image shieldImage;
    [SerializeField] private Image bulletImage;
    [SerializeField] private TMP_Text bulletText;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _poolCreator = GetComponent<PoolCreator>();

        _screenChecker = new ScreenChecker();
    }

    public void Initialize()
    {
        PlayerDead += OnPlayerDead;

        _poolCreator.Initialize();

        lives = removablesShipParts.Count;
        currentWave = -1;

        for (int i = 0; i < removablesShipParts.Count; i++)
        {
            removablesShipParts[i].SetActive(true);
            removablesShipParts[i].GetComponent<Collider>().enabled = false;
            removablesShipParts[i].GetComponent<Rigidbody>().useGravity = false;
            removablesShipParts[i].GetComponent<Rigidbody>().isKinematic = true;
        }

        for (int i = 0; i < livesImages.Count; i++)
        {
            livesImages[i].SetActive(true);
        }

        shootingTimer = fireRate;

        shieldImage.fillAmount = 1f;
        bulletImage.fillAmount = 1f;
        bulletText.text = "Current bullets: " + maxBullets.ToString();
    }

    private void Update()
    {
        if (!Globals.UI_IsOpen && Globals.GameStarted)
        {
            Movement();

            if (currentBulletsShooted < maxBullets)
                Shoot();
            else
                ReloadBullets();

            Shield();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        for (int i = 0; i < tagsToCollision.Count; i++)
        {
            if (collision.gameObject.tag == tagsToCollision[i])
            {
                if (!shieldActived)
                {
                    lives--;
                    if (lives < 0)
                    {
                        PlayerDead?.Invoke();
                        return;
                    }

                    shieldActived = true;
                    reloadingShield = false;
                    StartCoroutine(RemoveShipPart(lives));

                }
            }
        }
    }

    IEnumerator RemoveShipPart(int partId)
    {
        livesImages[partId].SetActive(false);
        removablesShipParts[partId].GetComponent<Collider>().enabled = true;
        removablesShipParts[partId].GetComponent<Rigidbody>().useGravity = true;
        //removablesShipParts[partId].GetComponent<Rigidbody>().isKinematic = false;

        yield return new WaitUntil(() => reloadingShield == true);
        removablesShipParts[partId].SetActive(false);
    }

    private void Movement()
    {
        float Horizontal = Input.GetAxis("Horizontal");
        float Vertical = Input.GetAxis("Vertical");

        _animator.SetFloat("Horizontal", Horizontal);
        _animator.SetFloat("Vertical", Vertical);

        if (_screenChecker.IsObjectVisible(Camera.main,objectToCheckLimit,screenLimit))
        {
            Vector3 destinationPos = new Vector3(Horizontal, Vertical, 0);

            if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
            {
                transform.position = Vector3.Lerp(transform.position, transform.position + destinationPos, shipSpeed * Time.deltaTime);
            }
        }
        else
        {
            transform.position = Vector3.Lerp(transform.position, Vector3.zero, backSmooth * Time.deltaTime);
        }
    }

    private void Shoot()
    {
        if (Input.GetKey("space"))
        {
            if (currentWave < 1)
                return;
            if (shootingTimer >= fireRate)
            {
                currenPositionIndex = 0;

                for (int i = 0; i < currentWave; i++)
                {
                    if (i <= shootPositions.Count)
                    {
                        GameObject bullet = _poolCreator.GetObjectFromPool();
                        if (bullet != null)
                        {
                            bullet.transform.position = shootPositions[currenPositionIndex].position;
                            currenPositionIndex++;
                            currentBulletsShooted++;
                        }
                    }
                }
                bulletText.text = "Current bullets: " + (maxBullets - currentBulletsShooted).ToString();
                bulletImage.fillAmount = 1 - ((1 / (float)maxBullets) * (float)currentBulletsShooted);
                shootingTimer = 0;
            }
            shootingTimer += Time.deltaTime;
        }
    }

    private void ReloadBullets()
    {
        // show UI Recharging
        reloadingTimer += Time.deltaTime;

        bulletImage.fillAmount = (1 / (float)timeToReload) * reloadingTimer;
        bulletText.text = "Reloading";

        if (reloadingTimer >= timeToReload)
        {
            currentBulletsShooted = 0;
            reloadingTimer = 0;
            bulletText.text = "Current bullets: " + maxBullets.ToString();
        }
    }

    private void Shield()
    {
        if (Input.GetKey(KeyCode.X) && !shieldActived && !reloadingShield)
        {
            shieldActived = true;
        }

        if (shieldActived && !reloadingShield)
        {
            shieldTimer += Time.deltaTime;
            shieldObject.SetActive(true);
            shieldImage.fillAmount = 1 - ((1 / timeToUseShield) * shieldTimer);

            if (shieldTimer >= timeToUseShield)
            {
                shieldTimer = 0;
                shieldObject.SetActive(false);
                shieldActived = false;
                reloadingShield = true;
            }
        }

        if (reloadingShield)
        {
            shieldTimer += Time.deltaTime;
            shieldImage.fillAmount = (1 / timeToReloadShield) * shieldTimer;

            if (shieldTimer >= timeToReloadShield)
            {
                shieldTimer = 0;
                reloadingShield = false;
            }
        }
    }

    private void OnPlayerDead()
    {
        //Show canvas dead  
        PlayerDead -= OnPlayerDead;
    }

    public void OnNewWave()
    {
        currentWave++;
    }
}
