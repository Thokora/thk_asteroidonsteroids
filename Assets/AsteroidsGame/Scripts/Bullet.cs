using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Bullet : ExplotionBehaviour
{
    [SerializeField] private float bulletLifeTime = 1f;

    private float timer;

    protected override void OnEnable()
    {
        base.OnEnable();
        timer = 0;
    }

    protected override void Update()
    {
        base.Update();
        if (canMove)
        {
            timer += Time.deltaTime;

            if (timer > bulletLifeTime)
            {
                timer = 0;
                objectImpacted?.Invoke(false);
            }
        }
    }

    protected override void OnObjectImpacted(bool showExplosionEffect)
    {
        base.OnObjectImpacted(showExplosionEffect);
    }
}
