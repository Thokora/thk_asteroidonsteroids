using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ExplotionBehaviour : MonoBehaviour
{
    /// <summary>
    /// Send true if you want to show a explosion effect
    /// </summary>
    public Action<bool> objectImpacted;

    [SerializeField] private GameObject model;
    [SerializeField] private float movementSpeed = 10;
    [SerializeField] protected Vector3 direction = Vector3.forward;
    [SerializeField] protected List<string> tagsToInteract = new List<string>();

    [Space, Header("Explosion settings")]
    [SerializeField] protected GameObject explotionObject;
    [SerializeField] protected float explosionDuration;
    [SerializeField] protected int impactToExploit = 1;

    protected int currentImpacts = 0;
    protected Rigidbody rb;
    protected Collider col;
    protected bool canMove;
    private ScreenChecker screenChecker;

    protected virtual void Awake()
    {
        screenChecker = new ScreenChecker();
        rb = GetComponent<Rigidbody>();
        col = GetComponent<Collider>();
    }

    protected virtual void OnEnable()
    {
        objectImpacted += OnObjectImpacted;
        if (explotionObject != null)
            explotionObject.SetActive(false);
        col.enabled = true;
        model.SetActive(true);
        currentImpacts = 0;
        canMove = true;
    }

    protected virtual void Update()
    {
        if (canMove)
        {
            rb.velocity = direction * movementSpeed;
        }

        if (!screenChecker.IsObjectVisible(Camera.main,model,-1.0f))
        {
            gameObject.SetActive(false);
        }
    }

    protected virtual void OnCollisionEnter(Collision collision)
    {
        for (int i = 0; i < tagsToInteract.Count; i++)
        {
            if (collision.gameObject.tag == tagsToInteract[i])
            {
                currentImpacts++;
                InteractWithTag(tagsToInteract[i]);
                objectImpacted?.Invoke(true);
                return;
            }
        }
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        for (int i = 0; i < tagsToInteract.Count; i++)
        {
            if (other.gameObject.tag == tagsToInteract[i])
            {
                currentImpacts++;
                InteractWithTag(tagsToInteract[i]);
                objectImpacted?.Invoke(true);
                return;
            }
        }
    }

    protected virtual void InteractWithTag(string tagImpacted)
    {

    }

    protected virtual void OnObjectImpacted(bool showExplosionEffect)
    {
        if (gameObject.activeInHierarchy)
            StartCoroutine(Touched(showExplosionEffect));
    }

    private IEnumerator Touched(bool showExplosionEffect)
    {
        if (currentImpacts == impactToExploit)
        {
            objectImpacted -= OnObjectImpacted;
            canMove = false;
            rb.velocity = Vector3.zero;
            col.enabled = false;
            model.SetActive(false);

            if (showExplosionEffect && explotionObject != null)
            {
                explotionObject.SetActive(true);
                yield return new WaitForSeconds(explosionDuration);
            }

            gameObject.SetActive(false);
        }
    }
}
