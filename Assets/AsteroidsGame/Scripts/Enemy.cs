using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent (typeof(Collider))]
public class Enemy : MonoBehaviour
{
    [SerializeField] private Collider col;

    private void Awake()
    {
        col = GetComponent<Collider>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Bullet"))
        {
            StartCoroutine(Touched());
        }
    }

    IEnumerator Touched()
    {
        col.enabled = false;
        yield return null;
        //Apagar collisionador
        //desactivar objecto enemy
        //prender particulas de explosion
        // despues de un tiempo desaparecer
    }

}
