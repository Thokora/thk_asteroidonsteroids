using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ScoreUIBehaviour : MonoBehaviour
{
    [SerializeField] private GameObject finalPanel;
    [SerializeField] private TMP_Text finalScoreText;
    [SerializeField] private TMP_Text scoreText;

    private void Awake()
    {
        if (finalPanel != null)
            finalPanel.SetActive(false);
    }

    private void Update()
    {
        if (Globals.GameStarted)
        {
            SetScore();
        }
    }

    private void SetScore()
    {
        if (scoreText != null)
            scoreText.text = "Score: " + ScoreManager.GetScore().ToString();
    }

    public void OnPlayerDead()
    {
        Globals.UI_IsOpen = true;

        if (finalPanel != null)
            finalPanel.SetActive(true);

        if (finalScoreText != null)
            finalScoreText.text = "Your score: " + ScoreManager.GetScore().ToString();
    }

    public void ResetUIValue()
    {
        ScoreManager.ResetScore();
        
        if (scoreText != null)
            scoreText.text = ScoreManager.GetScore().ToString();

        if (scoreText != null)
            scoreText.text = ScoreManager.GetScore().ToString();
    }
}
