using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolCreator : MonoBehaviour
{
    [SerializeField] private int amount;
    [SerializeField] private GameObject container;
    [SerializeField] private GameObject objectToPool;
    private List<GameObject> allObjects = new List<GameObject>();

    /// <summary>
    /// if values ara 0 or null, the values will be dafault values or the assigned values in editor
    /// </summary>
    public void SetManually(int amountObjs = 0, GameObject containerObjs = null, GameObject objToPool = null)
    {
        if (amountObjs != 0)
            amount = amountObjs;

        if (containerObjs != null)
            container = containerObjs;

        if (objectToPool != null)
            objectToPool = objToPool;
    }

    public void Initialize()
    {
        for (int i = 0; i < amount; i++)
        {
            GameObject objectCreated = Instantiate(objectToPool,container.transform);
            allObjects.Add(objectCreated);
        }
        DeactiveAllObjects();
    }

    public GameObject GetObjectFromPool()
    {
        for (int i = 0; i < amount; i++)
        {
            if (!allObjects[i].activeInHierarchy)
            {
                allObjects[i].SetActive(true);
                return allObjects[i];
            }
        }

        return null;
    }

    public void DeactiveAllObjects()
    {
        for (int i = 0; i < allObjects.Count; i++)
        {
            allObjects[i].SetActive(false);
        }
    }

    public List<GameObject> GetAllObjects()
    {
        return allObjects;
    }
}
